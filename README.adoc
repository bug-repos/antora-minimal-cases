= README / quickstart

This project shall be used to demonstrate some unexpected behaviours in Antora.

. Create a new branch for your case.
. Update antora-playbook.yml to use the newly created branch.
. Use the available NPM scripts:
** `npm i` to get the dependencies
** `npm build` to build the site one first time
** `npm serve` to serve the site at http://localhost:8080
** `npm build` (in another terminal) to build again. The site should be automatically refreshed in browser.
